import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  CUTaskResponse,
    DeleteResponse,
  Task,
  TaskResponse,
  User,
  UserResponse,
} from './types';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  baseUrl = 'https://devza.com/tests';
  taskUrl = `${this.baseUrl}/tasks`;
  getTasksUrl = `${this.taskUrl}/list`;
  createTaskUrl = `${this.taskUrl}/create`;
  deleteTaskUrl = `${this.taskUrl}/delete`;
  updateTaskUrl = `${this.taskUrl}/update`;
  getUsersUrl = `${this.taskUrl}/listusers`;

  headers = new HttpHeaders().set(
    'AuthToken',
    'TyAv2v8AQlQUaRlnZV17FFAXMkpahKOx'
  );

  getUsers(): Observable<UserResponse> {
    return this.http.get<UserResponse>(this.getUsersUrl, {
      headers: this.headers,
    });
  }

  createUpdateTask(task: Task): Observable<CUTaskResponse> {
    const formData = new FormData();
    for (let key in task) {
      if (task.hasOwnProperty(key)) {
        formData.append(key, task[key]);
      }
    }
    if (task.taskid) {
      return this.http.put<CUTaskResponse>(this.updateTaskUrl, formData, {
        headers: this.headers,
      });
    }
    return this.http.post<CUTaskResponse>(this.createTaskUrl, formData, {
      headers: this.headers,
    });
  }

  getTasks(): Observable<TaskResponse> {
    return this.http.get<TaskResponse>(this.getTasksUrl, {
      headers: this.headers,
    });
  }

  deleteTask(taskid: string): Observable<DeleteResponse> {
      const formData = new FormData();
      formData.append('taskid', taskid);
      return this.http.post<DeleteResponse>(this.deleteTaskUrl, formData, {headers: this.headers});
  }
}
