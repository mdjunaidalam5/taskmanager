import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.services';
import { User, UserResponse } from '../types';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private apiService: ApiService) { }
  users: User[] = [];
  ngOnInit(): void {
    this.apiService.getUsers().subscribe((data: UserResponse) => {
      console.log(data);
      this.users = data.users;
      
    })
  }

}
