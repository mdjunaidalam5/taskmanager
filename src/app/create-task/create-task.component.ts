import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.services';
import { Task, User, UserResponse } from '../types';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
})
export class CreateTaskComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router) {}
  date: string;
  time: string;
  users: User[] = [];
  assignedUser: User;
  isError: boolean = false;
  task: Task = {} as Task;
  showSuccessAlert: boolean;
  successAlertMessage: string;

  ngOnInit(): void {
    this.apiService.getUsers().subscribe((data: UserResponse) => {
      this.users = data.users;
      if (history.state.data) {
        console.log(history.state.data);
        this.task = history.state.data;
        this.task.taskid = this.task.id;
        this.updateAssignedUser();
        const dueDate = this.task.due_date.split(' ');
        this.date = dueDate[0];
        this.time = dueDate[1];
      } else {
        this.router.navigate(['/create-task']);
      }
    });
  }

  onDateKeyDown() {
    return false;
  }

  submitForm() {
    this.showSuccessAlert = false;
    console.log(this.task);
    if (
      !this.task.message ||
      !this.task.assigned_to ||
      !this.date ||
      !this.task.priority
    ) {
      this.isError = true;
      return;
    }

    if (!this.time) {
      const d = new Date();
      this.time = `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
    }
    this.successAlertMessage = this.task.taskid ? 'Task Updated' :'Task Created.'
    this.task.due_date = this.date + ' ' + this.time;
    this.apiService.createUpdateTask(this.task).subscribe(
      (data) => {
        console.log(data);
        if (data.status === 'success') {
          this.task.taskid = data.taskid;
          this.showSuccessAlert = true;
        } else {
          console.log(data);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateAssignedUser() {
    this.assignedUser = this.users.filter(
      (f) => f.id == this.task.assigned_to
    )[0];
  }

  assignUser(user: User) {
    console.log(user);
    this.assignedUser = user;
    this.task.assigned_to = this.assignedUser.id;
  }

  resetForm() {
    this.task = {} as Task;
    this.assignedUser = {} as User;
  }
}
