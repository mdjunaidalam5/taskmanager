import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { ApiService } from '../api.services';
import { DeleteResponse, Task, User } from '../types';

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.scss'],
})
export class TaskBoardComponent implements OnInit {
  constructor(private apiService: ApiService, private router: Router) {}
  tasks: Task[] = [];
  users: User[] = [];
  priorityFilter: string = 'all';
  tempTasks: Task[] = [];
  fromDateFilter: string;
  toDateFilter: string;

  ngOnInit(): void {
    forkJoin({
      tasksResponse: this.apiService.getTasks(),
      usersResponse: this.apiService.getUsers()
    }).subscribe((data)=> {
      console.log(data);
      if(data.usersResponse.status == 'success' && data.tasksResponse.status == 'success') {
        this.tasks = data.tasksResponse.tasks;
        this.users = data.usersResponse.users;
        this.tempTasks = [...this.tasks];
      }
      
    })
  }

  loadTasks() {
    this.apiService.getTasks().subscribe(
      (data) => {
        if (data.status === 'success') {
          this.tasks = data.tasks;
        }
      },
      (error) => {}
    );
  }

  deleteTask(taskid) {
    return this.apiService.deleteTask(taskid).subscribe(
      (data) => {
        if (data.status == 'success') {
          this.loadTasks();
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  onEdit(task: Task) {
    this.router.navigate(['/edit-task'], { state: { data: task } });
  }

  getUser(userid): User {
    return this.users.filter(f => f.id === userid)[0];
  }

  onPriorityFilterChange(prio) {
    console.log(prio);
    this.tasks = [...this.tempTasks];
    if(prio != 'all') {
      this.tasks = this.tasks.filter(t => t.priority === prio);
    }
  }

  onFromDateChange(date) {
    this.fromDateFilter = date;
    this.executeDateFilter();
  }

  onToDateChange(date) {
    this.toDateFilter = date;
    this.executeDateFilter();
  }

  private executeDateFilter() {
    if(this.toDateFilter && this.fromDateFilter) {
      const fromDate = new Date(this.fromDateFilter).getTime();
      const toDate = new Date(this.toDateFilter).getTime();
      this.tasks = this.tasks.filter((t) => {
        const dueDate = new Date(t.due_date.split(' ')[0]).getTime();
        return dueDate >= fromDate && dueDate <= toDate;
      })

    }
  }

  resetFilter() {
    this.toDateFilter = '';
    this.fromDateFilter = ''
    this.priorityFilter = 'all';
    this.tasks = [...this.tempTasks];
  }
}
