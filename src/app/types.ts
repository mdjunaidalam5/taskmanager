export interface User {
  id: string;
  name: string;
  picture: string;
}

export interface Task {
  message: string;
  due_date: string;
  priority: string;
  assigned_to: string;
  taskid?: string;
  id?: string;
}

export interface TaskResponse {
    status: string;
    tasks: Task[];
}

export interface UserResponse {
    status: string;
    users: User[];
}

export interface CUTaskResponse {
    taskid: string;
    status: string;
}

export interface DeleteResponse {
    status: string;
    message: string;
}